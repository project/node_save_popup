Node Save Popup
---------------------------

INTRODUCTION
-----------
  Set confirm modal popup on node save.

INSTALLATION:
-------------
  1. Install as you would normally install a contributed Drupal module. 
     See: https://www.drupal.org/node/3285065 for further information.

REQUIREMENTS
------------
  Nothing special is required.

CONFIGURATION
-------------
  1. Install the module "Node Save Popup".
  2. Go to the “Block Layout”. Eg:- Admin Menu >> structure >> block layout
  3. Go to your block region where you want to put it.
  4. Click the "Place block" button and in the modal dialog click the
     "Place block" button next to "Confirm Node Save Block".
  5. Click on the Save block button.
  6. Go to the “Configuration. Eg:-
     Admin Menu >> configuration >> services >> Node Save Configuration
  7. Click the content type checkboxes to enable popup based on types.
  8. Enter the text to be shown on popup.
  9. Click on the Save Configuration button.

UNINSTALLATION
--------------
  1. Uninstall as you would normally Uninstall a contributed Drupal module. 
     See: https://www.drupal.org/docs/user_guide/en/config-uninstall.html 
     for further information.
