<?php

namespace Drupal\node_save_popup\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;

/**
 * Provides the configuration form.
 *
 * @internal
 */
class SavePopupConfigForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * SavePopupConfigForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_save_popup_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'node_save_popup.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('node_save_popup.settings');

    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $options = [];
    foreach ($node_types as $node_type) {
      $options[$node_type->id()] = $node_type->label();
    }

    $form['confirm_save_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select list of content types to enable Confirm Save option'),
      '#options' => $options,
      '#default_value' => $config->get('confirm_save_types'),
    ];

    $form['confirm_save_title_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Confirm Save Title Text'),
      '#default_value' => $config->get('confirm_save_title_value'),
    ];

    $form['confirm_save_body_value'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Confirm Save Body Text'),
      '#default_value' => $config->get('confirm_save_body_value'),
    ];

    $form['confirm_save_yes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Confirm Save Button Text'),
      '#default_value' => $config->get('confirm_save_yes'),
    ];

    $form['confirm_save_cancel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Confirm Save Cancel Button Text'),
      '#default_value' => $config->get('confirm_save_cancel'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('node_save_popup.settings')
      ->set('confirm_save_types', $form_state->getValue('confirm_save_types'))
      ->set('confirm_save_title_value', $form_state->getValue('confirm_save_title_value'))
      ->set('confirm_save_body_value', $form_state->getValue('confirm_save_body_value'))
      ->set('confirm_save_yes', $form_state->getValue('confirm_save_yes'))
      ->set('confirm_save_cancel', $form_state->getValue('confirm_save_cancel'))
      ->save();
  }

}
