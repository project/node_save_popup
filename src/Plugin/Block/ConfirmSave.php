<?php

namespace Drupal\node_save_popup\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides a block with a confirm save text.
 *
 * @Block(
 *   id = "confirm_save_popup_block",
 *   admin_label = @Translation("Confirm Save Popup Block"),
 * )
 */
class ConfirmSave extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a ConfirmSave object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configFactory->get('node_save_popup.settings');
    $types = $config->get('confirm_save_types');
    $confirm_title = $config->get('confirm_save_title_value');
    $confirm_body = $config->get('confirm_save_body_value');
    $yes_text = $config->get('confirm_save_yes');
    $cancel_text = $config->get('confirm_save_cancel');

    return [
      '#theme' => 'confirm_save',
      '#attached' => [
        'library' => [
          'node_save_popup/node-save-popup',
        ],
      ],
      '#data' => [
        'types' => $types,
        'confirm_title_text' => $confirm_title,
        'confirm_body_text' => $confirm_body,
        'yes_text' => $yes_text,
        'no_text' => $cancel_text,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->getConfiguration();
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['node_save_popup_settings'] = $form_state->getValue('node_save_popup_settings');
  }

}
