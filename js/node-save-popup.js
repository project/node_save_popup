(function ($, Drupal) {
  Drupal.behaviors.node_save_popup = {
    attach: function (context, settings) {
      var userClickedYes = false;
      $(settings.savePopup).submit(function(e, submit){
      	if(!userClickedYes) {
      	  e.preventDefault();
      	  $("#confirmModal").modal('show');
      	}
      });

      $('button#confirm-yes').click(function(){
        userClickedYes = true;
        $(settings.savePopup).removeAttr('data-drupal-form-submit-last');
        $('.confirmsave').click();
      });

      $('.close-modal').click(function(){
        $('.jquery-modal').modal().hide();
        $('body').css('overflow','');
      });
    }
  };
})(jQuery, Drupal);
